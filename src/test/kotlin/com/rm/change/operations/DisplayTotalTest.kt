package com.rm.change.operations

import com.rm.change.model.DenominationType
import org.junit.Assert
import org.junit.Test

class DisplayTotalTest {
    private val balance = mutableMapOf(
        DenominationType.ONE to 12,
        DenominationType.TWO to 2,
        DenominationType.FIVE to 3,
        DenominationType.TEN to 4,
        DenominationType.TWENTY to 5
    )
    private val expectedPrintTotal = " 12 2 3 4 5"
    private val  displayTotal = DisplayTotal()

    @Test
    fun canDisplayTotals() {
        val result = displayTotal("12 2 3 4 5", balance)
        Assert.assertEquals(expectedPrintTotal, result)
    }
}