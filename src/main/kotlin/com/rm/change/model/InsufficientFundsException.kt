package com.rm.change.model

class InsufficientFundsException : Throwable(message = "Not enough money in the register")