package com.rm.change

import com.rm.change.model.DenominationType
import com.rm.change.model.DenominationType.*
import com.rm.change.model.InsufficientFundsException
import com.rm.change.model.OperationNotSupportedException
import com.rm.change.operations.AddFunds
import com.rm.change.operations.DisplayTotal
import com.rm.change.operations.MakeChange
import com.rm.change.operations.SubtractFunds
import org.junit.Assert.assertEquals
import org.junit.Test

class BankTest {
    private val balance = mutableMapOf(
        TWENTY to 1,
        TEN to 2,
        FIVE to 3,
        TWO to 4,
        ONE to 5
    )

    private val operations = mapOf(
        "put" to AddFunds(),
        "show" to DisplayTotal(),
        "take" to SubtractFunds(),
        "change" to MakeChange(SubtractFunds())
    )

    private val bank = Bank(balance, operations)

    @Test(expected = OperationNotSupportedException::class)
    fun operationNotSupported(){
        bank.executeOperation("fake")
    }

    @Test(expected = InsufficientFundsException::class)
    fun sampleInputDemonstration() {
        val showResult = bank.executeOperation("show")

        assertEquals("$68 1 2 3 4 5", showResult)
        assertEquals(1, balance[TWENTY])
        assertEquals(2, balance[TEN])
        assertEquals(3, balance[FIVE])
        assertEquals(4, balance[TWO])
        assertEquals(5, balance[ONE])

        val putResult = bank.executeOperation("put 1 2 3 0 5")

        assertEquals("$128 2 4 6 4 10", putResult)
        assertEquals(2, balance[TWENTY])
        assertEquals(4, balance[TEN])
        assertEquals(6, balance[FIVE])
        assertEquals(4, balance[TWO])
        assertEquals(10, balance[ONE])

        val takeResult = bank.executeOperation("take 1 4 3 0 10")

        assertEquals("$43 1 0 3 4 0", takeResult)
        assertEquals(1, balance[TWENTY])
        assertEquals(0, balance[TEN])
        assertEquals(3, balance[FIVE])
        assertEquals(4, balance[TWO])
        assertEquals(0, balance[ONE])

        val changeResult = bank.executeOperation("change 11")

        // due to the method that this algorithm makes change it does not produce the exact set of bills from the example
        assertEquals("$32 1 0 2 1 0", changeResult)
        assertEquals(1, balance[TWENTY])
        assertEquals(0, balance[TEN])
        assertEquals(2, balance[FIVE])
        assertEquals(1, balance[TWO])
        assertEquals(0, balance[ONE])

        // this will overdraw and should produce an exception
        bank.executeOperation("change 14")
    }
}