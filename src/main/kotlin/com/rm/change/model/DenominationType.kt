package com.rm.change.model

enum class DenominationType(val amount: Int) {
    TWENTY(20),
    TEN(10),
    FIVE(5),
    TWO(2),
    ONE(1);

    companion object {
        fun fromAmount(amount: Int) = DenominationType.values().first { it.amount == amount }
    }
}