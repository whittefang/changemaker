package com.rm.change.operations

import com.rm.change.MoneyUtil
import com.rm.change.model.DenominationType

class DisplayTotal : (String, MutableMap<DenominationType, Int>) -> String {
    override fun invoke(money: String, balance: MutableMap<DenominationType, Int>): String {
        return MoneyUtil.convertToString(balance)
    }
}