package com.rm.change.operations

import com.rm.change.model.DenominationType.*
import com.rm.change.model.InsufficientFundsException
import com.rm.change.model.InvalidInputException
import org.junit.Test
import org.junit.Assert.assertEquals
class MakeChangeTest {
    private val makeChange = MakeChange(SubtractFunds())

    @Test
    fun canMakeChange_Success_Eight() {
        val balance = mutableMapOf(
            ONE to 0,
            TWO to 4,
            FIVE to 1,
            TEN to 0,
            TWENTY to 0
        )

        makeChange.invoke("8", balance)

        assertEquals(0, balance[ONE])
        assertEquals(0, balance[TWO])
        assertEquals(1, balance[FIVE])
        assertEquals(0, balance[TEN])
        assertEquals(0, balance[TWENTY])
    }

    @Test
    fun canMakeChange_Success_exact_Eight() {
        val balance = mutableMapOf(
            ONE to 0,
            TWO to 4,
            FIVE to 0,
            TEN to 0,
            TWENTY to 0
        )

        makeChange.invoke("8", balance)

        assertEquals(0, balance[ONE])
        assertEquals(0, balance[TWO])
        assertEquals(0, balance[FIVE])
        assertEquals(0, balance[TEN])
        assertEquals(0, balance[TWENTY])
    }

    @Test
    fun canMakeChange_Success_exact_Zero() {
        val balance = mutableMapOf(
            ONE to 0,
            TWO to 4,
            FIVE to 0,
            TEN to 0,
            TWENTY to 0
        )

        makeChange.invoke("0", balance)

        assertEquals(0, balance[ONE])
        assertEquals(4, balance[TWO])
        assertEquals(0, balance[FIVE])
        assertEquals(0, balance[TEN])
        assertEquals(0, balance[TWENTY])
    }

    @Test(expected = InvalidInputException::class)
    fun canMakeChange_Success_negative() {
        val balance = mutableMapOf(
            ONE to 0,
            TWO to 4,
            FIVE to 0,
            TEN to 0,
            TWENTY to 0
        )

        makeChange.invoke("-10", balance)
    }

    @Test
    fun canMakeChange_Success_Ten() {
        val balance = mutableMapOf(
            ONE to 0,
            TWO to 4,
            FIVE to 0,
            TEN to 1,
            TWENTY to 0
        )

        makeChange.invoke("10", balance)

        assertEquals(0, balance[ONE])
        assertEquals(4, balance[TWO])
        assertEquals(0, balance[FIVE])
        assertEquals(0, balance[TEN])
        assertEquals(0, balance[TWENTY])
    }

    @Test(expected = InsufficientFundsException::class)
    fun canMakeChange_failure_ThrowsException() {
        val balance = mutableMapOf(
            ONE to 0,
            TWO to 2,
            FIVE to 0,
            TEN to 1,
            TWENTY to 0
        )
        makeChange.invoke("16", balance)


    }

    @Test(expected = InvalidInputException::class)
    fun canMakeChange_failure_negativeValue() {
        val balance = mutableMapOf(
            ONE to 0,
            TWO to 2,
            FIVE to 0,
            TEN to 1,
            TWENTY to 0
        )
        makeChange.invoke("-16", balance)


    }
}