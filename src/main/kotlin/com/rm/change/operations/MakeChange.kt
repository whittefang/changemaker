package com.rm.change.operations

import com.rm.change.model.DenominationType
import com.rm.change.model.InsufficientFundsException
import com.rm.change.model.InvalidInputException

class MakeChange(val subtractFunds: SubtractFunds) : (String, MutableMap<DenominationType, Int>) -> String {
    override fun invoke(amount: String, balance: MutableMap<DenominationType, Int>): String {
        val changeAmount = amount.toInt()
        if (changeAmount < 0) {
            throw InvalidInputException("negative values not allowed")
        }
        // find all possible bill combinations for a given amount
        val possibleBillCombinations = generateChangeSets(changeAmount, DenominationType.values().map { it.amount }, 0)
        // find bill combination that the bank can vend
        val validBillCombination =
            possibleBillCombinations.firstOrNull { subtractFunds.canRemoveMoneyMultiple(it, balance) }
                ?: throw InsufficientFundsException()
        return subtractFunds.remove(validBillCombination, balance)
    }

    private fun generateChangeSets(
        remainingAmount: Int,
        availableBills: List<Int>,
        currentIndex: Int
    ): List<MutableMap<DenominationType, Int>> {
        if (remainingAmount < 0) {
            //not a viable combination
            return listOf()
        }

        if (remainingAmount == 0) {
            // viable combination
            return listOf(mutableMapOf())
        }

        if (currentIndex == availableBills.size && remainingAmount > 0) {
            return listOf()
        }

        val left = generateChangeSets(remainingAmount - availableBills[currentIndex], availableBills, currentIndex)
        val right = generateChangeSets(remainingAmount, availableBills, currentIndex + 1)
        return addDenomination(DenominationType.fromAmount(availableBills[currentIndex]), left) + right
    }

    private fun addDenomination(
        denominationType: DenominationType,
        billsCollection: List<MutableMap<DenominationType, Int>>
    ): List<MutableMap<DenominationType, Int>> {
        billsCollection.forEach {
            val currentAmount = it.getOrDefault(denominationType, 0)
            it[denominationType] = currentAmount + 1
        }

        return billsCollection
    }

}