package com.rm.change.model

class InvalidInputException(message:String = "Invalid Input"):Throwable(message = message) {}