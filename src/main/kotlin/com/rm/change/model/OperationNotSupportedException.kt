package com.rm.change.model

class OperationNotSupportedException:Throwable(message = "given operation is not supported") {
}