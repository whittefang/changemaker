package com.rm.change

import com.rm.change.model.DenominationType
import com.rm.change.model.OperationNotSupportedException


class Bank(
    private val balance: MutableMap<DenominationType, Int> = mutableMapOf(),
    private val operations: Map<String, (String, MutableMap<DenominationType, Int>) -> String>
) {
    fun executeOperation(input: String): String {
        val operation = input.split(" ")[0]
        val operationInput = input.substringAfter(" ")
        val result = operations[operation]?.invoke(operationInput, balance) ?: throw OperationNotSupportedException()
        return "$${calculateTotal(balance)}$result"
    }


    private fun calculateTotal(balance: Map<DenominationType, Int>): Int {
        return balance.entries.fold(0) { acc, entry ->
            val currentTotal = entry.value
            val currentValue = entry.key.amount
            acc + (currentTotal * currentValue)
        }
    }
}