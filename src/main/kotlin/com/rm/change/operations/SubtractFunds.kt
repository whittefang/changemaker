package com.rm.change.operations

import com.rm.change.MoneyUtil
import com.rm.change.model.DenominationType
import com.rm.change.model.InsufficientFundsException

class SubtractFunds: (String, MutableMap<DenominationType, Int>) -> String {
    override fun invoke(money: String, balance: MutableMap<DenominationType, Int>): String {
        val moneyMap = MoneyUtil.convertToMap(money)
        return remove(moneyMap, balance)
    }

    fun remove(moneyMap: Map<DenominationType, Int>, balance: MutableMap<DenominationType, Int>): String {
        if (!canRemoveMoneyMultiple(moneyMap, balance)) throw InsufficientFundsException()
        moneyMap.forEach {
            remove(it.key, it.value, balance)
        }
        return MoneyUtil.convertToString(balance)
    }

    private fun remove(denominationType: DenominationType, amount: Int, balance: MutableMap<DenominationType, Int> ) {
        val currentAmount = balance[denominationType] ?: 0
        val adjustedTotal = currentAmount - amount
        if (adjustedTotal < 0) throw InsufficientFundsException()
        balance[denominationType] = adjustedTotal
    }

    fun canRemoveMoneyMultiple(money: Map<DenominationType, Int>, balance: MutableMap<DenominationType, Int>): Boolean {
        money.forEach {
            val currentAmount = balance[it.key] ?: 0
            if (currentAmount < it.value) {
                return false
            }
        }
        return true
    }
}