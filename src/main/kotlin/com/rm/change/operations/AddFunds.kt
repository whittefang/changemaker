package com.rm.change.operations

import com.rm.change.MoneyUtil
import com.rm.change.model.DenominationType

class AddFunds: (String, MutableMap<DenominationType, Int>) -> String {
    override fun invoke(money: String, balance: MutableMap<DenominationType, Int>): String {
        val moneyMap = MoneyUtil.convertToMap(money)
        moneyMap.forEach {
            add(it.key, it.value, balance)
        }
        return MoneyUtil.convertToString(balance)
    }

    private fun add(denominationType: DenominationType, amount: Int, balance: MutableMap<DenominationType, Int>) {
        val currentAmount = balance[denominationType] ?: 0
        val adjustedTotal = amount + currentAmount
        balance[denominationType] = adjustedTotal
    }
}