package com.rm.change

import com.rm.change.model.DenominationType
import org.junit.Assert
import org.junit.Test

class MoneyUtilTest{

    @Test
    fun canConvertToString(){
        val map = mapOf(DenominationType.TWENTY to 1,
            DenominationType.TEN to 2,
            DenominationType.FIVE to 3,
            DenominationType.TWO to 4,
            DenominationType.ONE to 5)
        val stringRepresentation = MoneyUtil.convertToString(map)
        Assert.assertEquals(" 1 2 3 4 5", stringRepresentation)
    }

    @Test
    fun canConvertToMap(){
        val mapRepresentation = MoneyUtil.convertToMap("1 2 3 4 5")
        Assert.assertEquals(1, mapRepresentation[DenominationType.TWENTY])
        Assert.assertEquals(2, mapRepresentation[DenominationType.TEN])
        Assert.assertEquals(3, mapRepresentation[DenominationType.FIVE])
        Assert.assertEquals(4, mapRepresentation[DenominationType.TWO])
        Assert.assertEquals(5, mapRepresentation[DenominationType.ONE])
    }
}