package com.rm.change.operations

import com.rm.change.model.DenominationType
import com.rm.change.model.InsufficientFundsException
import com.rm.change.model.InvalidInputException
import org.junit.Assert
import org.junit.Test

class SubtractFundsTest {
    private val subtractFunds = SubtractFunds()
    private val balance = mutableMapOf(
        DenominationType.TWENTY to 10,
        DenominationType.TEN to 10,
        DenominationType.FIVE to 10,
        DenominationType.TWO to 10,
        DenominationType.ONE to 10
    )

    @Test
    fun canSubtractFunds() {
        val money = "1 2 3 4 5"

        val result = subtractFunds.invoke(money, balance)

        Assert.assertEquals(" 9 8 7 6 5", result)
        Assert.assertEquals(9, balance[DenominationType.TWENTY])
        Assert.assertEquals(8, balance[DenominationType.TEN])
        Assert.assertEquals(7, balance[DenominationType.FIVE])
        Assert.assertEquals(6, balance[DenominationType.TWO])
        Assert.assertEquals(5, balance[DenominationType.ONE])
    }

    @Test(expected = InvalidInputException::class)
    fun canAddFunds_Failure_BadInput() {
        val money = "1 2 3 4 5 2 3 4 5"

        subtractFunds.invoke(money, balance)
    }

    @Test(expected = InsufficientFundsException::class)
    fun canAddFunds_Failure_NotEnoughMoney() {
        val money = "100 200 300 400 500"

        subtractFunds.invoke(money, balance)
    }

    @Test(expected = InvalidInputException::class)
    fun canAddFunds_Failure_negativeValues() {
        val money = "-1 0 0 0 0"

        subtractFunds.invoke(money, balance)
    }
}