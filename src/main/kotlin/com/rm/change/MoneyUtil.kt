package com.rm.change

import com.rm.change.model.DenominationType
import com.rm.change.model.InvalidInputException

class MoneyUtil {

    companion object {
        fun convertToMap(stringRepresentation: String): Map<DenominationType, Int> {
            val mapRepresentation = mutableMapOf<DenominationType, Int>()
            val totals = stringRepresentation.split(" ")
            val numberOfDenominations = DenominationType.values().size

            if (totals.size != numberOfDenominations) {
                throw InvalidInputException("incorrect number of denominations specified")
            }

            for (i in 0 until numberOfDenominations) {
                val total =  totals[i].toInt()
                if (total < 0) {throw InvalidInputException("negative values not allowed")}
                mapRepresentation[DenominationType.values()[i]] = total
            }

            return mapRepresentation
        }

        fun convertToString(mapRepresentation: Map<DenominationType, Int>):String {
            var stringRepresentation = ""

            mapRepresentation.forEach{
                stringRepresentation += " ${it.value}"
            }

            return stringRepresentation
        }
    }

}