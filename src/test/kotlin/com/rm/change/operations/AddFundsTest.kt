package com.rm.change.operations

import com.rm.change.model.DenominationType
import com.rm.change.model.InvalidInputException
import org.junit.Assert
import org.junit.Test

class AddFundsTest {
    private val addFunds = AddFunds()
    private val balance = mutableMapOf(
        DenominationType.TWENTY to 1,
        DenominationType.TEN to 1,
        DenominationType.FIVE to 1,
        DenominationType.TWO to 1,
        DenominationType.ONE to 1)

    @Test
    fun canAddFunds(){
        val money = "1 2 3 4 5"

        val result = addFunds.invoke(money, balance)

        Assert.assertEquals(" 2 3 4 5 6", result)
        Assert.assertEquals(2, balance[DenominationType.TWENTY])
        Assert.assertEquals(3, balance[DenominationType.TEN])
        Assert.assertEquals(4, balance[DenominationType.FIVE])
        Assert.assertEquals(5, balance[DenominationType.TWO])
        Assert.assertEquals(6, balance[DenominationType.ONE])
    }

    @Test(expected = InvalidInputException::class)
    fun canAddFunds_Failure_BadInput(){
        val money = "1 2 3 4 5 2 3 4 5"

        addFunds.invoke(money, balance)
    }

    @Test(expected = InvalidInputException::class)
    fun canAddFunds_Fails_negativeNumber(){
        val money = "-1 2 3 4 5"

        addFunds.invoke(money, balance)
    }
}